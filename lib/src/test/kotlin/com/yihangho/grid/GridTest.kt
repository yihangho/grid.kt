package com.yihangho.grid

import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertFails

val GRID = Grid(3, 4) { row, col -> row * 10 + col }

class GridTest {
  @Test fun testGetWithValidIndex() {
    assertEquals(GRID[0, 0], 0)
    assertEquals(GRID[1, 0], 10)
    assertEquals(GRID[2, 0], 20)

    assertEquals(GRID[0, 1], 1)
    assertEquals(GRID[1, 2], 12)
    assertEquals(GRID[2, 3], 23)
  }

  @Test fun testGetWithInvalidIndex() {
    assertFails { GRID[-1, 0] }
    assertFails { GRID[3, 0] }
    assertFails { GRID[0, -1] }
    assertFails { GRID[0, 4] }
    assertFails { GRID[-1, -1] }
    assertFails { GRID[3, 4] }
  }

  @Test fun testToNestedLists() {
    val expected = listOf(
      listOf(0, 1, 2, 3),
      listOf(10, 11, 12, 13),
      listOf(20, 21, 22, 23)
    )
    assertEquals(GRID.toNestedLists(), expected)
  }

  @Test fun testSubGridWithValidRanges() {
    val subGrid = GRID.subGrid(1..2, 0..2)
    assertEquals(subGrid.height, 2)
    assertEquals(subGrid.width, 3)

    val expected = listOf(
      listOf(10, 11, 12),
      listOf(20, 21, 22)
    )
    assertEquals(subGrid.toNestedLists(), expected)
  }

  @Test fun testSubGridWithInvalidRanges() {
    assertFails { GRID.subGrid(-1..2, 0..2) }
    assertFails { GRID.subGrid(1..3, 0..2) }
    assertFails { GRID.subGrid(1..2, -1..2) }
    assertFails { GRID.subGrid(1..2, 0..4) }
    assertFails { GRID.subGrid(-1..3, -1..4) }
  }

  @Test fun testRotateClockwise() {
    val expected = listOf(
      listOf(20, 10, 0),
      listOf(21, 11, 1),
      listOf(22, 12, 2),
      listOf(23, 13, 3)
    )
    assertEquals(GRID.rotateClockwise().toNestedLists(), expected)
  }

  @Test fun testRotateCounterClockwise() {
    val expected = listOf(
      listOf(3, 13, 23),
      listOf(2, 12, 22),
      listOf(1, 11, 21),
      listOf(0, 10, 20)
    )
    assertEquals(GRID.rotateCounterClockwise().toNestedLists(), expected)
  }

  @Test fun testReflectX() {
    val expected = listOf(
      listOf(20, 21, 22, 23),
      listOf(10, 11, 12, 13),
      listOf(0, 1, 2, 3)
    )
    assertEquals(GRID.reflectX().toNestedLists(), expected)
  }

  @Test fun testReflectY() {
    val expected = listOf(
      listOf(3, 2, 1, 0),
      listOf(13, 12, 11, 10),
      listOf(23, 22, 21, 20)
    )
    assertEquals(GRID.reflectY().toNestedLists(), expected)
  }
}
