package com.yihangho.grid

interface Grid<T> {
  val height: Int
  val width: Int

  operator fun get(row: Int, col: Int): T
}

fun <T> Grid(
  height: Int,
  width: Int,
  generator: (row: Int, col: Int) -> T
): Grid<T> = object : Grid<T> {
  override val height: Int = height
  override val width: Int = width

  override operator fun get(row: Int, col: Int): T {
    if (row !in 0..height-1) {
      throw IndexOutOfBoundsException("Row $row out of bounds for height $height")
    }
    if (col !in 0..width-1) {
      throw IndexOutOfBoundsException("Column $col out of bounds for width $width")
    }
    return generator(row, col)
  }
}

fun <T> Grid<T>.toNestedLists(): List<List<T>> {
  return List(height) { row ->
    List(width) { col -> get(row, col) }
  }
}

fun <T> Grid<T>.subGrid(rows: IntRange, cols: IntRange): Grid<T> {
  if (rows.start < 0 || rows.endInclusive >= height) {
    throw IndexOutOfBoundsException("Rows $rows out of bounds for height $height")
  }

  if (cols.start < 0 || cols.endInclusive >= width) {
    throw IndexOutOfBoundsException("Columns $cols out of bounds for width $width")
  }

  val newHeight = rows.count()
  val newWidth = cols.count()

  return Grid(newHeight, newWidth) { row, col -> get(row + rows.start, col + cols.start) }
}

fun <T> Grid<T>.rotateClockwise(): Grid<T> {
  return Grid(height=width, width=height) { row, col -> get(height - 1 - col, row) }
}

fun <T> Grid<T>.rotateCounterClockwise(): Grid<T> {
  return Grid(height=width, width=height) { row, col -> get(col, width - 1 - row) }
}

fun <T> Grid<T>.reflectX(): Grid<T> {
  return Grid(height, width) { row, col -> get(height - 1 - row, col) }
}

fun <T> Grid<T>.reflectY(): Grid<T> {
  return Grid(height, width) { row, col -> get(row, width - 1 - col) }
}
